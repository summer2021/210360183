package routers

import (
	"github.com/astaxie/beego"
	"github.com/vesoft-inc/nebula-http-gateway/controllers"
)

func init() {
	beego.Router("/", &controllers.DatabaseController{}, "*:Home")
	beego.Router("/api/db/connect", &controllers.DatabaseController{}, "POST:Connect")
	beego.Router("/api/db/exec", &controllers.DatabaseController{}, "POST:Execute")
	beego.Router("/api/db/disconnect", &controllers.DatabaseController{}, "POST:Disconnect")
	beego.Router("/index",&controllers.IndexController{},"*:Index")
	beego.Router("/spaces",&controllers.ApiController{},"GET:ShowSpaces")
	beego.Router("/spaces/:spaceName",&controllers.ApiController{},"POST:CreateNewSpace")
	beego.Router("/spaces/:spaceName",&controllers.ApiController{},"GET:DescribeSpace")
	beego.Router("/spaces/:spaceName",&controllers.ApiController{},"DELETE:DropSpace")
	beego.Router("/tags",&controllers.ApiController{},"GET:ShowTags")
	beego.Router("/tags/:tagName",&controllers.ApiController{},"POST:CreateNewTag")
	beego.Router("/tags/:tagName",&controllers.ApiController{},"GET:DescribeTag")
	beego.Router("/tags/:tagName",&controllers.ApiController{},"DELETE:DeleteTag")
	beego.Router("/tags/:tagName/add",&controllers.ApiController{},"POST:AddTagProperty")
	beego.Router("/tags/:tagName/change",&controllers.ApiController{},"POST:ChangeTagProperty")
	beego.Router("/tags/:tagName/drop",&controllers.ApiController{},"DELETE:DropTagProperty")
	beego.Router("/tags/:tagName/ttl",&controllers.ApiController{},"POST:TagPropertyTTL")
	beego.Router("/edges",&controllers.ApiController{},"GET:ShowEdges")
	beego.Router("/edges/:edgeTypeName",&controllers.ApiController{},"POST:CreateNewEdge")
	beego.Router("/edges/:edgeTypeName",&controllers.ApiController{},"GET:DescribeEdge")
	beego.Router("/edges/:edgeTypeName",&controllers.ApiController{},"DELETE:DeleteEdge")
	beego.Router("/edges/:edgeTypeName/addProperty",&controllers.ApiController{},"POST:AddEdgeProperty")
	beego.Router("/edges/:edgeTypeName/changeProperty",&controllers.ApiController{},"POST:ChangeEdgeProperty")
	beego.Router("/edges/:edgeTypeName/dropProperty",&controllers.ApiController{},"DELETE:DropEdgeProperty")
	beego.Router("/edges/:edgeTypeName/propertyTTL",&controllers.ApiController{},"POST:EdgePropertyTTL")
	beego.Router("/edges/:edgeTypeName/insert",&controllers.ApiController{},"POST:InsertEdge")
	beego.Router("/edges/:edgeTypeName/delete",&controllers.ApiController{},"DELETE:DeleteEdgeByID")
	beego.Router("/edges/:edgeTypeName/update",&controllers.ApiController{},"POST:UpdateEdges")
	beego.Router("/edges/:edgeTypeName/upsert",&controllers.ApiController{},"POST:UpsertEdges")
	beego.Router("/vertex/:VID",&controllers.ApiController{},"POST:InsertVertex")
	beego.Router("/vertex/:VID",&controllers.ApiController{},"DELETE:DeleteVertex")
	beego.Router("/vertex/:VID/update",&controllers.ApiController{},"POST:UpdateVertex")
	beego.Router("/vertex/:VID/upsert",&controllers.ApiController{},"post:UpsertVertex")

}
