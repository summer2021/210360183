package controllers

import "github.com/astaxie/beego"

type IndexController struct {
	beego.Controller
}

func (c *IndexController) Index(){
	var res Response
	res.Code = 0
	c.ServeJSON()
}

