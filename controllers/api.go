package controllers

import (
	"fmt"
	"github.com/astaxie/beego"
)

type ApiController struct {
	beego.Controller
}

func (c *ApiController) ShowSpaces(){
	var res Response
	rlt :="SHOW SPACES;"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) CreateNewSpace(){
	var res Response
	spaceName := c.Ctx.Input.Param(":spaceName")
	vidType := c.GetString("vid_type")
	partitionNum := c.GetString("partition_num")
	replicaFactor := c.GetString("replica_factor")
	if partitionNum == "" {
		partitionNum = "100"
	}
	if replicaFactor== "" {
		replicaFactor = "1"
	}
	if vidType== "" {
		vidType="FIXED_STRING(8)"
	}
	rlt :="CREATE SPACE "+spaceName+"(partition_num="+partitionNum+",replica_factor="+replicaFactor+",vid_type="+vidType+");"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) DescribeSpace(){
	var res Response
	spaceName := c.Ctx.Input.Param(":spaceName")
	rlt :="DESCRIBE SPACE "+spaceName+";"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) DropSpace(){
	var res Response
	spaceName := c.Ctx.Input.Param(":spaceName")
	rlt :="DROP SPACE "+spaceName+";"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) ShowTags(){
	var res Response
	rlt :="SHOW TAGS;"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) CreateNewTag(){
	var res Response
	tagName := c.Ctx.Input.Param(":tagName")
	propName := c.GetString("prop_name")
	dataType := c.GetString("data_type")
	DEFAULT := c.GetString("DEFAULT")
	TtlDuration := c.GetString("TTL_DURATION")
	TtlCol := c.GetString("TTL_COL")
	rlt :=""
	if propName =="" {
		rlt = "CREATE TAG no_property(); "
	}else{
		rlt="CREATE TAG "+tagName+"("+propName+" "+dataType
		if DEFAULT!=""{
			rlt+=" DEFAULT " + DEFAULT
		}
		rlt+=")"
		if TtlDuration!=""{
			rlt+=" TTL_DURATION ="+TtlDuration
		}
		if TtlCol!=""{
			rlt+=", TTL_COL =\""+TtlCol+"\""
		}
		rlt+=";"
	}
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) DescribeTag(){
	var res Response
	tagName := c.Ctx.Input.Param(":tagName")
	rlt :="DESCRIBE TAG "+tagName+";"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) DeleteTag(){
	var res Response
	tagName := c.Ctx.Input.Param(":tagName")
	rlt :="DROP TAG "+tagName+";"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) AddTagProperty(){
	var res Response
	tagName := c.Ctx.Input.Param(":tagName")
	propName := c.GetString("prop_name")
	dataType := c.GetString("data_type")
	rlt :="ALTER TAG "+tagName+" ADD ("+ propName +" "+ dataType +");"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) ChangeTagProperty(){
	var res Response
	tagName := c.Ctx.Input.Param(":tagName")
	propName := c.GetString("prop_name")
	dataType := c.GetString("data_type")
	rlt :="ALTER TAG "+tagName+" CHANGE ("+ propName +" "+ dataType +");"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) DropTagProperty(){
	var res Response
	tagName := c.Ctx.Input.Param(":tagName")
	propName := c.GetString("prop_name")
	dataType := c.GetString("data_type")
	rlt :="ALTER TAG "+tagName+" DROP ("+ propName +" "+ dataType +");"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) TagPropertyTTL(){
	var res Response
	tagName := c.Ctx.Input.Param(":tagName")
	TtlDuration := c.GetString("TTL_DURATION")
	TtlCol := c.GetString("TTL_COL")
	rlt :="ALTER TAG "+tagName+" TTL_DURATION="+ TtlDuration +", TTL_COL=\""+ TtlCol +"\");"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) ShowEdges(){
	var res Response
	rlt :="SHOW EDGES;"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) CreateNewEdge(){
	var res Response
	edgeTypeName := c.Ctx.Input.Param(":edgeTypeName")
	propName := c.GetString("prop_name")
	dataType := c.GetString("data_type")
	DEFAULT := c.GetString("DEFAULT")
	TtlDuration := c.GetString("TTL_DURATION")
	TtlCol := c.GetString("TTL_COL")
	rlt :=""
	if propName ==""{
		rlt ="CREATE EDGE no_property(); "
	} else{
		rlt="CREATE EDGE "+edgeTypeName+"("+propName+" "+dataType
		if DEFAULT!=""{
			rlt+=" DEFAULT " + DEFAULT
		}
		rlt+=")"
		if TtlDuration!=""{
			rlt+=" TTL_DURATION ="+TtlDuration
		}
		if TtlCol!=""{
			rlt+=", TTL_COL =\""+TtlCol+"\""
		}
		rlt+=";"
	}
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) DescribeEdge(){
	var res Response
	edgeTypeName := c.Ctx.Input.Param(":edgeTypeName")
	rlt :="DESCRIBE EDGE "+edgeTypeName+";"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) DeleteEdge(){
	var res Response
	edgeTypeName := c.Ctx.Input.Param(":edgeTypeName")
	rlt :="DROP EDGE "+edgeTypeName+";"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) AddEdgeProperty(){
	var res Response
	edgeTypeName := c.Ctx.Input.Param(":edgeTypeName")
	propName := c.GetString("prop_name")
	dataType := c.GetString("data_type")
	rlt :="ALTER EDGE "+edgeTypeName+" ADD ("+ propName +" "+ dataType +");"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) ChangeEdgeProperty(){
	var res Response
	edgeTypeName := c.Ctx.Input.Param(":edgeTypeName")
	propName := c.GetString("prop_name")
	dataType := c.GetString("data_type")
	rlt :="ALTER EDGE "+edgeTypeName+" CHANGE ("+ propName +" "+ dataType +");"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) DropEdgeProperty(){
	var res Response
	edgeTypeName := c.Ctx.Input.Param(":edgeTypeName")
	propName := c.GetString("prop_name")
	rlt :="ALTER EDGE "+edgeTypeName+" DELETE ("+ propName +");"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) EdgePropertyTTL(){
	var res Response
	edgeTypeName := c.Ctx.Input.Param(":edgeTypeName")
	TtlDuration := c.GetString("TTL_DURATION")
	TtlCol := c.GetString("TTL_COL")
	rlt :="ALTER EDGE "+edgeTypeName+" TTL_DURATION="+ TtlDuration +", TTL_COL=\""+ TtlCol +"\";"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) InsertEdge(){
	var res Response
	edgeTypeName := c.Ctx.Input.Param(":edgeTypeName")
	srcVid := c.GetString("src_vid")
	dstVid := c.GetString("dst_vid")
	propNameList := c.GetString("prop_name_list")
	propValueList := c.GetString("prop_value_list")
	rank:= c.GetString("rank")
	rlt :="INSERT EDGE " + edgeTypeName + " (" + propNameList + ") VALUES \"" + srcVid + "\"->\"" + dstVid + "\""
	if rank!="" {
		rlt+="@"+rank
	}
	rlt+=":(" + propValueList + ");"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) DeleteEdgeByID(){
	var res Response
	edgeTypeName := c.Ctx.Input.Param(":edgeTypeName")
	srcVid := c.GetString("src_vid")
	dstVid := c.GetString("dst_vid")
	rank:= c.GetString("rank")
	rlt :="DELETE EDGE "+edgeTypeName+" \""+ srcVid + "\"->\"" + dstVid + "\""
	if rank!=""{
		rlt+="@"+rank
	}
	rlt+=";"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) UpdateEdges(){
	var res Response
	edgeTypeName := c.Ctx.Input.Param(":edgeTypeName")
	srcVid := c.GetString("src_vid")
	dstVid := c.GetString("dst_vid")
	rank:= c.GetString("rank")
	SET:= c.GetString("SET")
	WHEN:= c.GetString("WHEN")
	YIELD:= c.GetString("YIELD")
	rlt := "UPDATE EDGE ON " + edgeTypeName + " \"" + srcVid + "\"->\"" + dstVid + "\""
	if rank!=""{
		rlt+="@"+rank
	}
	rlt+=" SET "+ SET
	if WHEN!=""{
		rlt+=" WHEN "+WHEN
	}
	if YIELD!=""{
		rlt+=" YIELD "+YIELD
	}
	rlt+=";"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) UpsertEdges(){
	var res Response
	edgeTypeName := c.Ctx.Input.Param(":edgeTypeName")
	srcVid := c.GetString("src_vid")
	dstVid := c.GetString("dst_vid")
	rank:= c.GetString("rank")
	SET:= c.GetString("SET")
	WHEN:= c.GetString("WHEN")
	YIELD:= c.GetString("YIELD")
	rlt := "UPDATE EDGE ON " + edgeTypeName + " \"" + srcVid + "\"->\"" + dstVid + "\""
	if rank!=""{
		rlt+="@"+rank
	}
	rlt+=" SET "+ SET
	if WHEN!=""{
		rlt+=" WHEN "+WHEN
	}
	if YIELD!=""{
		rlt+=" YIELD "+YIELD
	}
	rlt+=";"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) InsertVertex(){
	var res Response
	VID := c.Ctx.Input.Param(":VID")
	tagName := c.GetString("tag_name")
	propNameList := c.GetString("prop_name_list")
	propValueList := c.GetString("prop_value_list")
	rlt :="INSERT VERTEX " + tagName + " (" + propNameList + ") VALUES \"" + VID + "\":("+propValueList+");"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) DeleteVertex(){
	var res Response
	VID := c.Ctx.Input.Param(":VID")
	rlt :="DELETE VERTEX " + VID+";"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) UpdateVertex(){
	var res Response
	VID := c.Ctx.Input.Param(":VID")
	tagName := c.GetString("tag_name")
	SET:= c.GetString("SET")
	WHEN:= c.GetString("WHEN")
	YIELD:= c.GetString("YIELD")
	rlt := "UPDATE VERTEX ON " + tagName+" \""+VID+"\""
	rlt+=" SET "+ SET
	if WHEN!=""{
		rlt+=" WHEN "+WHEN
	}
	if YIELD!=""{
		rlt+=" YIELD "+YIELD
	}
	rlt+=";"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}

func (c *ApiController) UpsertVertex(){
	var res Response
	VID := c.Ctx.Input.Param(":VID")
	tagName := c.GetString("tag_name")
	SET:= c.GetString("SET")
	WHEN:= c.GetString("WHEN")
	YIELD:= c.GetString("YIELD")
	rlt := "UPSERT VERTEX ON " + tagName+" \""+VID+"\""
	rlt+=" SET "+ SET
	if WHEN!=""{
		rlt+=" WHEN "+WHEN
	}
	if YIELD!=""{
		rlt+=" YIELD "+YIELD
	}
	rlt+=";"
	fmt.Println(rlt)
	res.Code = 0
	c.ServeJSON()
}



